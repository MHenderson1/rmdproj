
0.1.0 / 2018-01-31
==================

  * New function: publishReadme.
  * Rename package and template: rmdproj
  * Add a parameter for author.
  * Add a setup block.
  * Add an .Rmd template.
  * Update installation instructions.
  * add simple README
  * initial commit
