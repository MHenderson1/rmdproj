# This function showcases how one might write a function to be used as an
# RStudio project template. This function will be called when the user invokes
# the New Project wizard using the project template defined in the template file
# at:
#
#   inst/rstudio/templates/project/hello_world.dcf
#
# The function itself just echos its inputs and outputs to a file called INDEX,
# which is then opened by RStudio when the new project is opened.
rmdproj <- function(path, ...) {

  # ensure path exists
  dir.create(path, recursive = TRUE, showWarnings = FALSE)
  rmarkdown_path <- file.path(path, paste0(basename(path), ".Rmd"))
  file.create(rmarkdown_path)

  dots <- list(...)

  header <- c(
   "---",
   paste("title:", dots[["title"]]),
   paste("author:", dots[["author"]]),
   "date: \"`r format(Sys.time(), '%d %B %Y')`\"",
   "output:",
   "  github_document:",
   "    toc: yes",
   "---"
  )

  setup <- c(
    "```{r init, include = FALSE, echo = FALSE}",
    "knitr::opts_chunk$set(",
    "  cache = FALSE,",
    "  cache.path = \"cache/\",",
    "  warning = FALSE,",
    "  message = FALSE,",
    "  fig.path = \"figure/\",",
    "  fig.width = 6,",
    "  fig.height = 4",
    ")",
    "ggplot2::theme_set(hrbrthemes::theme_ipsum_rc())",
    "```"
  )

  report <- c(
    "",
    "## Introduction"
  )

  # collect inputs
  text <- lapply(seq_along(dots), function(i) {
    key <- names(dots)[[i]]
    val <- dots[[i]]
    a <- paste0(key, ": ", val)
    paste0(path, a)
  })

  # collect into single text string
  contents <- paste(
    paste(header, collapse = "\n"),
    paste(setup, collapse = "\n"),
    paste(report, collapse = "\n"),
    sep = "\n"
  )

  # write to index file
  writeLines(contents, con = rmarkdown_path)

}
